# Wine-Demo
wine-demo for wineapp

## 安装
```bash
# 方法一（git 远程安装）
wineapp -i https://jihulab.com/wineapp/wine-demo.git

# 方法二 (中心库 远程安装)
wineapp -i com.demo.wine

# 方法三 本地安装
git clone https://jihulab.com/wineapp/wine-demo.git
cd wine-demo
wineapp -i
```

## 中心库
- https://jihulab.com/wineapp/winecenter.git
```bash
# 搜索
wineapp search com.demo.wine
```

## 文件说明
```bash
.
├── app
│   ├── entries（入口目录）
│   │   ├── applications（桌面快捷方式）
│   │   │   └── com.demo.wine.desktop
│   │   └── icons（应用图标）
│   │       └── com.demo.wine.svg
│   ├── files（入口文件）
│   │   └── entry.sh
│   └── info（应用权限）
├── APPINFO（应用信息）
└── README.md（文档说明）
```

- **APPINFO** 文件说明
```bash
# 应用名称
APP_NAME=""

# 应用版本
APP_VER=""

# 安装文件名（不含 .exe）
APP_INSTALLER=""

# 应用包名
APP_PKG_NAME=""

# 应用下载地址
APP_DOWNLOAD_URL=""

# 静默安装参数
INSTALL_QUIET="/S"
```

- **app/files/entry.sh** 入口文件
> 一般情况下，只需要修改此部分 (`init()`) 即可
```bash
init() {
    # 应用名称
    APP_NAME="Wine-Demo"

    # 应用版本
    APP_VER="1.0.0"

    # 应用启动文件（不可修改）
    START_SHELL_PATH="${HOME}/.wineapp/tools/run.sh"

    # 应用启动文件
    EXEC_PATH="c:/Program Files (x86)/Demo/Demo.exe"
    
    # 应用卸载文件
    UNINSTALL_PATH="c:/Program Files (x86)/Demo/Uninstall.exe"

    # 静默安装参数
    INSTALL_QUIET="/S"
```